# Domande Internet Security 2020 Sessione Estiva

## Primo appello
Parte più delicata di IPSec  
Come funziona il meccanismo a finestra di IPSec  
Chaffing and Winnowing  
Approcci steganografici  
iptables  
Trojan  
Trojan d'esempio  
Tassonomia software nocivo (classificazione del software)  
Differenza tra SSL e TLS  
Funzioni hash in SSL  
Seconda parte di WATA2  
VPN  
VPN con IPSec  
Openvpn  
VPN con TLS  
HTTPS  
IDS e Firewall  
Proprietà di sicurezza di secondo livello  
TLS handshake  
Attacchi statistici  
Proprietà di non ripudio in WATA2  

## Secondo Appello
Firma digitale  
Differenza tra timestamp e nonce  
Esempio di protocollo che utilizza Nonce  
P.H function  
Campi ESP  
Meccanismo a finestra di IPSec  
Differenza tra tunnel mode e transport mode in IPSec  
Cos'è la crittografia  
Proprietà di correttezza e di sicurezza di un crittosistema nel caso simmetrico e asimmetrico  
Differenza tra cifratura e firma digitale  
N-S simmetrico  
Differenza tra crittografia e sicurezza  
Protocollo AH  
SA IPSec  
crypt(3)  
Certificazione chiavi  
Alternative alla crittografia  
Autenticazione e anonimato in WATA2  
Woo Lam  
Vulnerabilità e fix di Woo Lam  
Quando un crittosistema è sicuro?  
PRF  
